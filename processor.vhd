library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity processor is
port(
	Clk         : in  std_logic;
	Reset       : in  std_logic;
	-- Instruction memory
	I_Addr      : out std_logic_vector(31 downto 0);
	I_RdStb     : out std_logic;
	I_WrStb     : out std_logic;
	I_DataOut   : out std_logic_vector(31 downto 0);
	I_DataIn    : in  std_logic_vector(31 downto 0);
	-- Data memory
	D_Addr      : out std_logic_vector(31 downto 0);
	D_RdStb     : out std_logic;
	D_WrStb     : out std_logic;
	D_DataOut   : out std_logic_vector(31 downto 0);
	D_DataIn    : in  std_logic_vector(31 downto 0)
);
end processor;

architecture processor_arq of processor is 

component ALU 
    port  (a : in STD_LOGIC_VECTOR (31 downto 0);
           b : in STD_LOGIC_VECTOR (31 downto 0);
           control : in STD_LOGIC_VECTOR (2 downto 0);
           zero : out STD_LOGIC;
           result : out STD_LOGIC_VECTOR (31 downto 0));
           
end component;

component registers 
    port  (clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           wr : in STD_LOGIC;
           reg1_dr : in STD_LOGIC_VECTOR (4 downto 0);
           reg2_dr : in STD_LOGIC_VECTOR (4 downto 0);
           reg_wr : in STD_LOGIC_VECTOR (4 downto 0);
           data_wr : in STD_LOGIC_VECTOR (31 downto 0);
           data1_rd : out STD_LOGIC_VECTOR (31 downto 0);
           data2_rd : out STD_LOGIC_VECTOR (31 downto 0));
           
end component;

--DECLARACION DE SE?ALES--
      --ETAPA IF--
signal PC: std_logic_vector (31 downto 0);
signal PC_4: std_logic_vector (31 downto 0);
signal PCSRC: std_logic;
signal next_PC: std_logic_vector (31 downto 0);

      --ETAPA ID-- 
signal ID_data1_rd: std_logic_vector (31 downto 0);
signal ID_data2_rd: std_logic_vector (31 downto 0);
signal ID_PC_4: std_logic_vector (31 downto 0);
signal ID_Instruction: std_logic_vector (31 downto 0);
signal ID_RegWrite: std_logic; --necesario para habilitar escritura en registro. 
signal ID_MemToReg: std_logic; --necesario para definir si escribo valor del AluResult o de la memoria.
signal ID_MemRead: std_logic; --necesario para habilitar lectura de memoria para LW.
signal ID_MemWrite: std_logic; --necesario para habilitar escritura en memoria para SW.
signal ID_RegDst: std_logic; --necesario para determinar el registro destino (rt o rd)
signal ID_AluOp: std_logic_vector(2 downto 0); --define la operación que se hace en la ALU o si la operación se define desde los bits de la instrucción
signal ID_ALUSrc: std_logic; --determina si la segunda entrada de la ALU viene del immediate con extension de signo o de rt.
signal ID_immediate: std_logic_vector (31 downto 0); --necesario para definir el inmediato del LUI, ya con extension de signo considerada.
signal ID_rt: std_logic_vector (4 downto 0); --rt, que es instruction(20:16)
signal ID_rd: std_logic_vector (4 downto 0); --rd, que es instruction(15:11)
signal ID_BranchEQ:  std_logic; --necesario para definir si la instruccion es BEQ.
signal ID_BranchNEQ:  std_logic; --necesario para definir si la instruccion es BNE.


      --ETAPA EX--
signal EX_data1_rd: std_logic_vector (31 downto 0);
signal EX_data2_rd: std_logic_vector (31 downto 0); --lo necesito ya que este valor puede entrar por la segunda entrada de la ALU.
signal ALUResult: std_logic_vector (31 downto 0); 
signal zero: std_logic;
signal ALUControl: std_logic_vector (2 downto 0);
signal ALU_B_In: std_logic_vector (31 downto 0);
signal EX_RegWrite: std_logic; --paso ID_RegWrite 
signal EX_MemToReg: std_logic; --paso ID_MemToReg
signal EX_MemRead: std_logic; --paso ID_MemRead
signal EX_MemWrite: std_logic; --paso EX_MemWrite
signal RegDst: std_logic; --paso ID_RegDst y defino registro destino en esta etapa
signal ALUOp: std_logic_vector(2 downto 0); --paso ID_ALUop y defino que operacion realizo en esta etapa
signal ALUSrc: std_logic; --paso ID_ALUSrc y defino cual es mi segunda entrada a la ALU en esta etapa
signal EX_immediate: std_logic_vector (31 downto 0); --paso ID_immediate
signal EX_rt: std_logic_vector (4 downto 0); --paso ID_rt
signal EX_rd: std_logic_vector (4 downto 0); --paso ID_rd
signal EX_Instruction_RegDst: std_logic_vector (4 downto 0); --emula al MUX de RegDst, le paso el registro destino
signal EX_PC_4: std_logic_vector (31 downto 0); --paso ID_PC_4, necesario para el calculo de la direccion de salto
signal EX_PC_Branch: std_logic_vector (31 downto 0); --para calcular la direccion de salto
signal EX_BranchEQ:  std_logic; --paso ID_BranchEQ
signal EX_BranchNEQ:  std_logic; --paso ID_BranchNEQ

      
      
      --ETAPA MEM--
signal MEM_PC_Branch: std_logic_vector (31 downto 0);
signal MEM_RegWrite: std_logic;
signal MEM_MemToReg: std_logic;
signal MEM_AluResult: std_logic_vector (31 downto 0);
signal MEM_Instruction_RegDst: std_logic_vector (4 downto 0);
signal MEM_MemRead: std_logic;
signal MEM_MemWrite: std_logic;
signal MEM_data2_rd: std_logic_vector (31 downto 0);
signal BranchEQ: std_logic; --paso EX_BranchEQ y paso su valor para definir salto
signal BranchNEQ: std_logic; --paso EX_BranchNEQ y paso su valor para definir salto
signal MEM_Zero: std_logic; --paso zero y define cual de los Branch pasa para definir salto

     
      --ETAPA WB--
signal WB_reg_wr: std_logic_vector (4 downto 0);
signal WB_data_wr: std_logic_vector (31 downto 0);
signal MemToReg: std_logic;
signal RegWrite: std_logic;
signal WB_AluResult: std_logic_vector (31 downto 0);
signal WB_MemData: std_logic_vector (31 downto 0);
       
        
begin 	
---------------------------------------------------------------------------------------------------------------
-- ETAPA IF
---------------------------------------------------------------------------------------------------------------
 -- Contador de programa
 PCCount: process(clk, reset)
  begin
    if reset = '1' then
      PC <= ( others =>'0');
    elsif rising_edge(clk) then
      PC <= next_PC;
    end if;
  end process;

  PC_4 <= PC + 4;
  next_PC <= PC_4 when (PCSRC = '0') else
            MEM_PC_Branch;
	
-- Interfaz con memoria de Instrucciones
  I_Addr <= PC;
  I_RdStb <= '1';
  I_WrStb <= '0';
  I_DataOut <= (others => '0');
 
---------------------------------------------------------------------------------------------------------------
-- REGISTRO DE SEGMENTACION IF/ID
--------------------------------------------------------------------------------------------------------------- 
 IF_ID : process(clk,reset)
  begin
    if reset ='1' then
      ID_PC_4 <= (others => '0');
      ID_Instruction <= (others => '0');
    elsif rising_edge(clk) then
      ID_PC_4 <= PC_4;
      ID_Instruction <= I_DataIn;
    end if;
  end process;
 
 
---------------------------------------------------------------------------------------------------------------
-- ETAPA ID
---------------------------------------------------------------------------------------------------------------
-- Instanciacion del banco de registros
Registers_inst:  registers 
	Port map (
			clk => clk, 
			reset => reset, 
			wr => RegWrite, 
			reg1_dr => ID_Instruction(25 downto 21), 
			reg2_dr => ID_Instruction( 20 downto 16), 
			reg_wr => WB_reg_wr, 
			data_wr => WB_data_wr , 
			data1_rd => ID_data1_rd ,
			data2_rd => ID_data2_rd ); 

-- UNIDAD DE CONTROL

 CONTROL_UNIT: process (ID_Instruction) --defino unidad de control pasando el codigo de la instruccion
   begin
      case ID_Instruction(31 downto 26) is --corroboro los 6 bits mas significativos para determinar el tipo de instruccion
      --Tipo R--
      when "000000" => 
				ID_RegWrite<= '1'; --habilito escritura en registro
				ID_MemToReg<= '0'; --elijo ALUResult
				ID_MemRead<= '0';  --no leo de memoria
				ID_MemWrite<= '0'; --no escribo en memoria
				ID_BranchEQ<= '0'; --no es BEQ
	 			ID_BranchNEQ<= '0'; --no es BNE
				ID_RegDst<= '1'; --elijo rd
				ID_AluOp<= "010"; --defino operacion en ALU
				ID_ALUSrc<= '0'; --elijo data2_rd
                
      --LW--
      when "100011" => 
				ID_RegWrite<= '1'; --escribo en registro
				ID_MemToReg<= '1'; --elijo memoria de datos
				ID_MemRead<= '1'; --leo memoria
				ID_MemWrite<= '0'; --no escribo en memoria
				ID_BranchEQ<= '0'; --no BEQ
	 			ID_BranchNEQ<= '0'; -- no BNE
				ID_RegDst<= '0'; --elijo rt
				ID_AluOp<= "000"; --operacion en ALU
				ID_ALUSrc<= '1'; --elijo immediate con extension de signo (aca es offset)
                
      --SW--
      when "101011" => 
				ID_RegWrite<= '0'; --es dont care en realidad
				ID_MemToReg<= '0'; --elijo ALUResult
				ID_MemRead<= '0'; --no leo memoria
				ID_MemWrite<= '1'; --escribo en memoria
				ID_BranchEQ<= '0'; --no BEQ
	 			ID_BranchNEQ<= '0'; --no BNE
				ID_RegDst<= '0'; --elijo rt
				ID_AluOp<= "000"; --operacion en ALU
				ID_ALUSrc<= '1'; --elijo immediate con extension de signo (aca es offset)
      --BEQ--
      when "000100" => 
				ID_RegWrite<= '0'; --es dont care en realidad
				ID_MemToReg<= '0'; --es dont care en realidad
				ID_MemRead<= '0'; --no leo memoria
				ID_MemWrite<= '0'; --no escribo en memoria
				ID_BranchEQ<= '1'; --es BEQ
	 			ID_BranchNEQ<= '0'; --no es BNE
				ID_RegDst<= '0'; --elijo rt
				ID_AluOp<= "001"; --operacion en ALU
				ID_ALUSrc<= '0'; --elijo data2_rd
      --LUI--
      when "001111" => 
				ID_RegWrite<= '1'; --escribo en registro
				ID_MemToReg<= '0'; --elijo ALUResult
				ID_MemRead<= '0'; --no leo en memoria pero no importaria si leyera tampoco
				ID_MemWrite<= '0'; --no escribo en memoria
				ID_BranchEQ<= '0'; --no BEQ
	 			ID_BranchNEQ<= '0'; --no BNE
				ID_RegDst<= '0'; --elijo rt porque la definicion de la instruccion lo requiere
				ID_AluOp<= "111"; --operacion en ALU
				ID_ALUSrc<= '1'; --elijo inmediato con extension de signo
                
      --BNE--
      when "000101" =>
      				ID_RegWrite<= '0'; --es todo igual al BEQ
				ID_MemToReg<= '0'; 
				ID_MemRead<= '0'; 
				ID_MemWrite<= '0'; 
				ID_BranchEQ<= '0'; --excepto aca
	 			ID_BranchNEQ<= '1'; --y aca 
				ID_RegDst<= '0'; 
				ID_AluOp<= "001"; 
				ID_ALUSrc<= '0';
                
      when others => 
				ID_RegWrite<= '0'; 
				ID_MemToReg<= '0'; 
				ID_MemRead<= '0'; 
				ID_MemWrite<= '0'; 
				ID_BranchEQ<= '0';
	 			ID_BranchNEQ<= '0'; 
				ID_RegDst<= '0'; 
				ID_AluOp<= "000"; 
				ID_ALUSrc<= '0';
    end case;
  end process;
  
ID_immediate <= (x"FFFF"&ID_Instruction(15 downto 0)) when ID_Instruction(15) = '1' else (x"0000"&ID_Instruction(15 downto 0)); --recordar que ID_Instruction(15 downto 0)) es el immediate de la instruccion. Aca hago el sign extend tambien.
ID_rt <= ID_Instruction(20 downto 16); 
ID_rd <= ID_Instruction(15 downto 11);

    

---------------------------------------------------------------------------------------------------------------
-- REGISTRO DE SEGMENTACION ID/EX
---------------------------------------------------------------------------------------------------------------

ID_EX: process(clk, reset)
  begin 
    if (reset = '1' ) then --si hay reset, pongo todas las instrucciones en 0
      EX_PC_4 <= (others =>'0');
      
      EX_data1_rd <= (others =>'0');
      EX_data2_rd <= (others =>'0');
      EX_RegWrite <= '0' ;
      EX_MemToReg <= '0' ; 
      EX_MemRead <=  '0' ;
      EX_MemWrite <=  '0' ;
      EX_BranchEQ <=  '0' ;
      EX_BranchNEQ <=  '0' ;
      RegDst <=  '0' ;
      ALUOp <= (others =>'0');
      ALUSrc <= '0' ;
      EX_immediate <= (others => '0');
      EX_rt <= (others => '0');
      EX_rd <= (others => '0');
    elsif( rising_edge (clk)) then --si hay flanco ascendente, paso de un registro de segmentacion a otro
      EX_data1_rd <= ID_data1_rd;
      EX_data2_rd <= ID_data2_rd;
      EX_RegWrite <= ID_RegWrite;
      EX_MemToReg <= ID_MemToReg;
      EX_MemRead <= ID_MemRead;
      EX_MemWrite <= ID_MemWrite;
      EX_BranchEQ <=  ID_BranchEQ;
      EX_BranchNEQ <=  ID_BranchNEQ; 
      RegDst <= ID_RegDst;
      ALUOp <= ID_AluOp;
      ALUSrc <= ID_ALUSrc;
      EX_immediate <= ID_immediate;
      EX_rt <= ID_rt;
      EX_rd <= ID_rd;
      EX_PC_4 <= ID_PC_4;
    end if;
end process;
 
---------------------------------------------------------------------------------------------------------------
-- ETAPA EX
---------------------------------------------------------------------------------------------------------------
-- Instanciacion de ALU
ALU_inst: alu 
	port map(
		a => EX_data1_rd, 
		b => ALU_B_In, 
      control => AluControl,
      zero => zero, 
		result => ALUResult);

-- Unidad de Control de ALU
controlAlu : process(EX_immediate, AluOp) --necesito el immediate y aluop
begin 
   case (AluOp) is 
      when "000" =>
         AluControl <= "010"; -- Load, Store
      when "001" =>
         AluControl <= "110"; -- BEQ, BNE
      when "010" => -- Tipo R. Para estas, al tener el mismo codigo, reviso los 6 bits menos significativos
         if EX_immediate(5 downto 0) = "100000" then
            AluControl <= "010"; -- ADD
         elsif EX_immediate(5 downto 0) = "100010" then
            AluControl <= "110"; --SUB
         elsif EX_immediate(5 downto 0) = "100100" then
            AluControl <= "000"; --AND
         elsif EX_immediate(5 downto 0) = "100101" then
            AluControl <= "001"; --OR
         elsif EX_immediate(5 downto 0) = "101010" then
            AluControl <= "111"; --SLT
         else 
            AluControl <= "000";
         end if;
      when "111" =>
         AluControl <= "100"; -- LUI
      when others => 
         AluControl <= "000";
   end case;
end process;

-- MUX ALUSrc
ALU_B_In <= EX_data2_rd when ALUSrc = '0' else EX_immediate; -- la entrada B de la ALU sera o el read data 2 de los registros o el inmediato con la extension de signo

-- MUX RegDst
EX_Instruction_RegDst <= EX_rt when RegDst  = '0' else EX_rd; -- defino si el registro donde escribo sera rt o rd
  
-- Calculo de direcci?n de salto
EX_PC_Branch <= (EX_PC_4)+(EX_immediate(29 downto 0)&"00"); -- tomo el PC+4, le sumo el inmediato con extension de signo y le concateno dos 0 al final, lo que equivale a hacer un shift left de 2 posiciones.

---------------------------------------------------------------------------------------------------------------
-- REGISTRO DE SEGMENTACION EX/MEM
---------------------------------------------------------------------------------------------------------------

EX_MEM: process(clk, reset)
  begin 
    if (reset = '1' ) then --si hay reset, hago todo 0
      MEM_RegWrite <= '0';
      MEM_MemToReg <= '0';
      BranchEQ <= '0';
      BranchNEQ <= '0';
      MEM_MemRead <= '0';
      MEM_MemWrite <= '0';
      MEM_Zero <= '0';
      MEM_AluResult <= (others => '0');
      MEM_data2_rd <= (others => '0');
      MEM_Instruction_RegDst <= (others => '0');
		MEM_PC_Branch <= (others => '0');
	elsif( rising_edge (clk)) then --si no, hago pase de un registro a otro
      MEM_RegWrite <= EX_RegWrite;
      MEM_MemToReg <= EX_MemToReg;
      BranchEQ <= EX_BranchEQ;
      BranchNEQ <= EX_BranchNEQ;
      MEM_MemRead <= EX_MemRead;
      MEM_MemWrite <= EX_MemWrite;
      MEM_Zero <= Zero;
      MEM_AluResult <= ALUResult;
      MEM_data2_rd <= EX_data2_rd;
      MEM_Instruction_RegDst <= EX_Instruction_RegDst;
		MEM_PC_Branch <= EX_PC_Branch; 
    end if;
end process;


---------------------------------------------------------------------------------------------------------------
-- ETAPA MEM
---------------------------------------------------------------------------------------------------------------

D_Addr <= MEM_AluResult;
D_RdStb <= MEM_MemRead;
D_WrStb <= MEM_MemWrite;
D_DataOut <= MEM_data2_rd;

--calculo de condici?n de salto
PCSRC <= BranchEQ when MEM_Zero = '1' else BranchNEQ; --si zero es 1, pasa BranchEQ. Si zero es 0, pasa BranchNEQ


---------------------------------------------------------------------------------------------------------------
-- REGISTRO DE SEGMENTACION MEM/WB
---------------------------------------------------------------------------------------------------------------
EX_MEM_ETAPA: process(clk, reset)
  begin 
    if (reset = '1' ) then 
		 RegWrite <= '0';
		 MemToReg <= '0' ; 
		 WB_MemData <= (others => '0');
		 WB_AluResult <= (others => '0');
		 WB_reg_wr <= (others => '0');
	elsif( rising_edge (clk)) then
		 RegWrite <= MEM_RegWrite;
		 MemToReg <= MEM_MemToReg;  
		 WB_MemData <= D_DataIn;
		 WB_AluResult <= MEM_AluResult; 
		 WB_reg_wr <= MEM_Instruction_RegDst;
    end if;
end process;

---------------------------------------------------------------------------------------------------------------
-- ETAPA WB
---------------------------------------------------------------------------------------------------------------
WB_data_wr <= WB_AluResult when MemToReg = '0' else 
					WB_MemData ; 


end processor_arq;

